/*
 * bitBangI2c.cpp
 *
 *  Created on: 12 Nov 2019
 *      Author: Paris Moschovakos
 *
 *  This program provides an example on how to use IoBatch to do I2C 
 *  transactions. It was created to facilitate NSW electronics integration 
 *  courtesy of ATLAS Central DCS team.
 */

#include <ClientSessionFactory.h>
#include <uaplatformlayer.h>
#include <iomanip>
#include <LogIt.h>
#include <boost/program_options.hpp>
#include <IoBatch.h>
#include <bitset>

using namespace boost::program_options;
using namespace UaoClientForOpcUaSca;

constexpr uint8_t I2C_READ_BIT = 0x01;
constexpr uint8_t I2C_WRITE_BIT = 0x00;
constexpr uint8_t I2C_10_BIT_ADDRESSING = 0xF0;
constexpr unsigned I2C_DELAY = 2; // in us -> 100KHz I2C

void initializeI2c( IoBatch& ioBatch, unsigned int scl, unsigned int sda )
{

  ioBatch.addSetPins( { { scl, true }, { sda, true } } );
  ioBatch.addSetPinsDirections( { { scl, IoBatch::OUTPUT }, { sda, IoBatch::OUTPUT } }, 10 );

}

void startCondition( IoBatch& ioBatch, unsigned int scl, unsigned int sda )
{

	ioBatch.addSetPins( { { scl, true }, { sda, true } }, I2C_DELAY );
	ioBatch.addSetPins( { { sda, false } }, I2C_DELAY );
	ioBatch.addSetPins( { { scl, false } }, I2C_DELAY );

}

void stopCondition( IoBatch& ioBatch, unsigned int scl, unsigned int sda )
{

	ioBatch.addSetPins( { { sda, false } }, I2C_DELAY );
	ioBatch.addSetPins( { { scl, true } }, I2C_DELAY );
	ioBatch.addSetPins( { { sda, true } }, I2C_DELAY );

}

void getAcknowledge( IoBatch& ioBatch, unsigned int scl, unsigned int sda )
{

  ioBatch.addSetPinsDirections( { { sda, IoBatch::INPUT } } );
  ioBatch.addSetPins( { { sda, false } } );
  ioBatch.addSetPins( { { scl, true } }, I2C_DELAY );
  ioBatch.addGetPins();
  ioBatch.addSetPins( { { scl, false } }, I2C_DELAY );
  ioBatch.addSetPinsDirections( { { sda, IoBatch::OUTPUT } } );

}

void i2cTransmitByte( IoBatch& ioBatch, unsigned int scl, unsigned int sda, uint8_t byte )
{

  for( auto i = 0; i < 8; ++i )
  {

      if ( byte & 0x80 )
        ioBatch.addSetPins( { { sda, true } } );
      else
        ioBatch.addSetPins( { { sda, false } } );

      byte <<= 1;

      ioBatch.addSetPins( { { scl, true } }, I2C_DELAY );
      ioBatch.addSetPins( { { scl, false } }, I2C_DELAY );

  }
	  
  getAcknowledge( ioBatch, scl, sda );

}

void i2cReceiveByte( IoBatch& ioBatch, unsigned int scl, unsigned int sda, bool last )
{

	ioBatch.addSetPinsDirections( { { sda, IoBatch::INPUT } } );

  for ( auto i = 0; i < 8; ++i )
  {

	  ioBatch.addSetPins( { { scl, true } }, I2C_DELAY );
	  ioBatch.addGetPins();
	  ioBatch.addSetPins( { { scl, false } }, I2C_DELAY );
    
  }

  // Send ACK if it is the last byte to read
  if( last )
	  ioBatch.addSetPins( { { sda, true } }, I2C_DELAY );
  else
	  ioBatch.addSetPins( { { sda, false } }, I2C_DELAY );

  ioBatch.addSetPinsDirections( { { sda, IoBatch::OUTPUT } } );
  ioBatch.addSetPins( { { scl, true } }, I2C_DELAY );
  ioBatch.addSetPins( { { scl, false } }, I2C_DELAY );

}

struct Config
{

    std::string opcUaEndpoint;
    std::string	bbAddress;
    int		      i2cScl;
    int		      i2cSda;
    int         i2cRegisterAddress;
    std::string logLevelStr;

};

Config parseProgramOptions(int argc, char* argv[])
{
  using namespace boost::program_options;
  Config config;

  options_description options("Options");

  Log::LOG_LEVEL logLevel;

  options.add_options()
          ("help,h",          "show help")
          ("version,v",       "show version")
          ("endpoint,e",      value<std::string>(&config.opcUaEndpoint)->default_value("opc.tcp://127.0.0.1:48020"),  "OPC UA Endpoint, e.g. opc.tcp://127.0.0.1:48020" )
          ("address,a",       value<std::string>(&config.bbAddress)->default_value("MMFE8_0007.gpio.bitBanger"),       "Address of the BitBanger")
          ("SCL,c",		        value<int>(&config.i2cScl)->default_value(17), "GPIO pin that corresponds to SCL")
          ("SDA,d",		        value<int>(&config.i2cSda)->default_value(18), "GPIO pin that corresponds to SDA")
          ("Register,r",		  value<int>(&config.i2cRegisterAddress)->default_value(0), "ROC register address to be read")
          ("trace_level,t",   value<std::string>(&config.logLevelStr)->default_value("INF"),                         "Trace level, one of: ERR,WRN,INF,DBG,TRC")
          ;


  variables_map vm;

  store( parse_command_line (argc, argv, options), vm );
  notify (vm);
  if (vm.count("help"))
  {
      std::cout << options << std::endl;
      exit(1);
  }
  if (vm.count("version"))
  {
    	std::cout << "1.0.0" << std::endl;
    	exit(1);
	}
  if (! Log::logLevelFromString( config.logLevelStr, logLevel ) )
  {
      std::cout << "Log level not recognized: '" << config.logLevelStr << "'" << std::endl;
      exit(1);
  }

    return config;
}

int main( int argc, char* argv[])
{

    Config config = parseProgramOptions(argc, argv);

    UaPlatformLayer::init();

    Log::initializeLogging(Log::INF);

    UaClientSdk::UaSession* session = ClientSessionFactory::connect( config.opcUaEndpoint.c_str() );
    if (!session)
        return -1;

    auto scl = config.i2cScl;
    auto sda = config.i2cSda;

    if ( config.i2cRegisterAddress > 127 && config.i2cRegisterAddress < 0 )
      throw std::out_of_range( "Invalid register address. Allowed values between 0-127" );

    LOG(Log::INF) << "Reading register: " << config.i2cRegisterAddress;

    // Create a new batch of IO requests object
    IoBatch ioBatch( session, UaNodeId( config.bbAddress.c_str(), 2 ) );

    // Initialize the lines
    initializeI2c( ioBatch, scl, sda );

    // Prepare the I2C-GPIO batch
    startCondition( ioBatch, scl, sda );
    i2cTransmitByte( ioBatch, scl, sda, I2C_10_BIT_ADDRESSING | I2C_READ_BIT );
    i2cTransmitByte( ioBatch, scl, sda, (uint8_t) config.i2cRegisterAddress );
    i2cReceiveByte( ioBatch, scl, sda, true );
    stopCondition( ioBatch, scl, sda );

    // Dispatch the batch and collect the replies that correspond to SDA
    auto interestingPinSda = repliesToPinBits( ioBatch.dispatch(), sda );

    // Print them out
    LOG(Log::INF) << "1st byte ACK: 0b" << interestingPinSda[0];
    LOG(Log::INF) << "2nd byte ACK: 0b" << interestingPinSda[1];

    std::bitset<8> registerValue;

    for ( auto i = 0; i < 8; ++i )
	{
		registerValue[7-i] = interestingPinSda[i+2];
	}

    LOG(Log::INF) << "Register contents: 0x" << std::setfill('0') << std::setw(2) << std::hex << registerValue.to_ulong();

    ServiceSettings sessset = ServiceSettings();
    session->disconnect(sessset, OpcUa_True);
    delete session;

}
