project(UAO_SCA_DEMO CXX)
cmake_minimum_required(VERSION 2.8)

add_definitions( -std=c++11 )

include_directories(
	${PROJECT_BINARY_DIR}/deploy/open62541-compat/include/	
	${PROJECT_BINARY_DIR}/deploy/LogIt/include
	../include/
	)

file(GLOB SOURCES ../src/*.cpp ../src/*.cc)

message( ${PROJECT_BINARY_DIR}/deploy/open62541-compat/lib/libopen62541-compat.a)

add_library( this OBJECT ${SOURCES})

add_executable(
	demo
	demo.cpp
	$<TARGET_OBJECTS:this>
	)

add_executable(
        demo_adc_samples
        demo_adc_samples.cpp
        $<TARGET_OBJECTS:this>
        )
        
add_executable(
        demo_send_bitfile
        demo_send_bitfile.cpp
        $<TARGET_OBJECTS:this>
        )

add_executable(
	demo_bitBanger
	demo_bitBanger.cpp
	$<TARGET_OBJECTS:this>
	)

add_executable(
	bitBangI2c
	bitBangI2c.cpp
	$<TARGET_OBJECTS:this>
	)

set(COMMON_LIBS
	${PROJECT_BINARY_DIR}/deploy/open62541-compat/lib/libopen62541-compat.a
	${PROJECT_BINARY_DIR}/deploy/LogIt/lib/libLogIt.a
        -lboost_program_options-mt
	-lprotobuf
	)

target_link_libraries(
	demo
	${COMMON_LIBS}
	)

target_link_libraries(
        demo_adc_samples
	${COMMON_LIBS}
        )

target_link_libraries(
        demo_send_bitfile
        ${COMMON_LIBS}
        )

target_link_libraries(
	demo_bitBanger
	${COMMON_LIBS}
	)

target_link_libraries(
	bitBangI2c
	${COMMON_LIBS}
	)