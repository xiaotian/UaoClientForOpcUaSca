#pragma once

#include <iostream>
#include <uaclient/uaclientsdk.h>

namespace UaoClientForOpcUaSca
{

using namespace UaClientSdk;



class GpioBitBanger
{

public:

    GpioBitBanger(
        UaSession* session,
        UaNodeId objId
    );

// getters, setters for all variables

    void bitBang(
        const UaByteString &   in_requestsMessageSerialized,std::vector<OpcUa_UInt32> & out_readRegisterData

    );


private:

    UaSession  * m_session;
    UaNodeId     m_objId;

};



}

